﻿using Eliminacode.core;
using Eliminacode.models;
using Eliminacode.repositories;
using Eliminacode.viewmodels.core;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Eliminacode.viewmodels
{

    public class ServiceCollectionViewModel : ViewModelBaseCollection<Service,ServiceViewModel> {

        #region PROPERTIES

        /// <summary>
        /// Il "Service" selezionato 
        /// </summary>
        public ServiceViewModel ServiceSelect {
            get => _ServiceSelect;
            set {
                if ( value == _ServiceSelect ) return;
                if (_ServiceSelect != null ) value.IsSelected = false;
                _ServiceSelect = value;
                if (_ServiceSelect != null) value.IsSelected = true;

                this.OnPropertyChanged();
            }
        }
        ServiceViewModel _ServiceSelect;

        #endregion


        #region COMMANDS

        public ICommand UpdateCommand { get =>
            new Command(async () => {
                var repo = new RepositoryBase<Service>();
                Model = await repo.GetAll();
            });
        }

        public ICommand RemoveCommand { get =>
            new Command<ServiceViewModel>(async (serviceVM) => {
                if ( serviceVM == null ) return;
                var repo = new RepositoryService();
                await repo.Remove(serviceVM.Model);
                var index = Model.FindIndex((service) => service.Id == serviceVM.Model.Id);
                if ( index!=-1) {
                    Model.RemoveAt ( index );
                    ViewModelUpdate();
                }
            });
        }

        public ICommand SaveCommand { get =>
            new Command<ServiceViewModel>(async (serviceVM) => {
                var repo = new RepositoryService();
                await repo.Save(serviceVM.Model);
                if (serviceVM.Model.Id==null ) {
                    Model.Add(serviceVM.Model);
                } else {
                    var index = Model.FindIndex ( (service) => service.Id == serviceVM.Model.Id );
                    if ( index!=-1 ) Model[index] = serviceVM.Model;
                }
                ViewModelUpdate();
            });
        }


        #endregion

    }

}
