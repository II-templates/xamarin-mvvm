﻿using Eliminacode.models;
using Eliminacode.repositories;
using Eliminacode.viewmodels.core;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using Xamarin.Forms;

namespace Eliminacode.viewmodels
{
    public class ServiceViewModel : ViewModelBase<Service> {



        #region PROPERTIES

        public string Name {
            get => Model?.Name;
            set {
                if ( value == Model?.Name ) return;
                Model.Name = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsSelected {
            get => _IsSelected;
            set {
                if (value.Equals(_IsSelected)) return;
                this._IsSelected = value;
                this.OnPropertyChanged();
            }
        }
        bool _IsSelected;

        #endregion



        #region COMMANDS

        public ICommand CommandSelect {
            get => new Command( () => {
                IsSelected = true; 
            });
        }

        public ICommand CommandSave {
            get => new Command( async () => {
                var repo = new RepositoryService();
                await repo.Save( Model );
            });
        }

        public ICommand CommandDelete {
            get => new Command( async () => {
                var repo = new RepositoryService();
                await repo.Remove( Model );
            });
        }

        #endregion

    }

}
