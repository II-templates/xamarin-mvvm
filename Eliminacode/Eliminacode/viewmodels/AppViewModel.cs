﻿using Eliminacode.models;
using Eliminacode.repositories;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using Eliminacode.viewmodels.core;

namespace Eliminacode.viewmodels
{
    public class AppViewModel : ViewModelBase<AppModel> {


        #region PROPERTIES [VIEW-MODEL <-> VIEW]

        public string Title {
            get => Model?.Title;
            set {
                if ( value == Model?.Title ) return;
                Model.Title = value;
                this.OnPropertyChanged();
            }
        }
        


        public ServiceCollectionViewModel Services { 
            get {
                if ( _Services == null ) {
                    _Services = new ServiceCollectionViewModel();
                }
                return _Services;
            }
        }
        ServiceCollectionViewModel _Services;



        public ServiceViewModel ServiceSelect {
            get => _ServiceSelect;
            set {
                if (value == _ServiceSelect) return;
                _ServiceSelect = value;
                ServiceInEditCreate();
                this.OnPropertyChanged();
            }
        }
        ServiceViewModel _ServiceSelect;



        public ServiceViewModel ServiceInEdit {
            get => _ServiceInEdit;
            set {
                if (value == _ServiceInEdit) return;
                _ServiceInEdit = value;
                this.OnPropertyChanged();
            }
        }
        ServiceViewModel _ServiceInEdit;
        void ServiceInEditCreate () {
            ServiceInEdit = _ServiceSelect == null ? null : new ServiceViewModel() {
                Model = ServiceSelect.Model.Clone() as Service
            };
        }

        #endregion



        #region COMMANDS [VIEW -> VIEW-MODEL] [VIEW-MODEL -> VIEW-MODEL]

        public ICommand ServiceCreateCommand {
            get => new Command( () => {
                ServiceInEdit = new ServiceViewModel() {
                    Model = new Service() { Name = "<new>" }
                };
            });
        }

        public ICommand ServiceSaveCommand {
            get => new Command<ServiceViewModel>((serviceVM) => {
                ServiceSelect = null;
                Services.SaveCommand.Execute(serviceVM);
            });
        }

        public ICommand ServiceRemoveCommand {
            get => new Command<ServiceViewModel>((serviceVM) => {
                ServiceSelect = null;
                Services.RemoveCommand.Execute(serviceVM);
            });
        }

        public ICommand ServiceCancelCommand {
            get => new Command<ServiceViewModel>((serviceVM) => {
                ServiceInEditCreate();
            });
        }

        #endregion

    }
}
