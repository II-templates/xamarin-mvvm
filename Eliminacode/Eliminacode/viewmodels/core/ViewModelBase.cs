﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Eliminacode.viewmodels.core
{
    /// <summary>
    /// La classe base del "ViewModel".
    /// In pratica contiene il riferimento al "Model"
    /// 
    /// </summary>
    /// <typeparam name="T_MODEL"></typeparam>
    public class ViewModelBase<T_MODEL> : ViewModelNotify, IViewModel<T_MODEL> {

        /// <summary>  
        ///  E' il "Model" che questo "ViewModel" deve gestire 
        ///  E' un oggetto POCO
        /// </summary>  
        public T_MODEL Model {
            get => _Model;
            set {
                if (value.Equals(this._Model)) return;
                var old = _Model;
                _Model = value;
                OnModelChanged (_Model,old);
            }
        }
        T_MODEL _Model = default(T_MODEL);

        virtual protected void OnModelChanged ( T_MODEL newValue, T_MODEL oldValue ) {
            OnPropertyChanged("Model");
        }

    }
}
