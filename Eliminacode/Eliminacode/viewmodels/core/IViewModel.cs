﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eliminacode.viewmodels.core
{
    public interface IViewModel<T_MODEL> {
        T_MODEL Model { get; set; }
    }
}
