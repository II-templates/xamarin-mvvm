﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Eliminacode.viewmodels.core
{
    public class ViewModelBaseCollection<T_MODEL,T_VIEW_MODEL> : ViewModelNotify where T_VIEW_MODEL : IViewModel<T_MODEL>, new() {



        /// <summary>
        /// E' l'array di "Model" che questo ViewModel deve gestire.
        /// </summary>
        public IList<T_MODEL> Model {
            get => _Model;
            set {
                if (value == this._Model) return;
                var old = _Model;
                _Model = value;
                OnModelChanged(_Model, old);
            }
        }
        virtual protected void OnModelChanged(IList<T_MODEL> current, IList<T_MODEL> old){
            ViewModelUpdate();
            OnPropertyChanged("Model");
        }
        IList<T_MODEL> _Model = default(IList<T_MODEL>);



        /// <summary>
        /// E' la lista di "ViewModel" da gestire per la questa "Model Collection"
        /// Sono generati automaticamente da "VievModelUpdate"
        /// </summary>
        public ObservableCollection<T_VIEW_MODEL> ViewModelChildren {
            get => _ViewModelChildren;
            set {
                if (value.Equals(this._ViewModelChildren)) return;
                ObservableCollection<T_VIEW_MODEL> old = _ViewModelChildren;
                _ViewModelChildren = value;
                OnViewModelChildrenChanged(old, _ViewModelChildren);
            }
        }
        virtual protected void OnViewModelChildrenChanged(ObservableCollection<T_VIEW_MODEL> old, ObservableCollection<T_VIEW_MODEL> current){
            OnPropertyChanged("ViewModelChildren");
        }
        ObservableCollection<T_VIEW_MODEL> _ViewModelChildren = null;



        /// <summary>
        /// Permette di allineare la "Collection" di "ViewModel" con l'array di "Model"
        /// [II] da ottimizzare. Ora cancello tutti i viewmodel e li ricreo invece potrei recuperare quelli gia' esistenti
        /// </summary>
        public virtual void ViewModelUpdate () {
            ViewModelChildren = Model!=null 
                ? new ObservableCollection<T_VIEW_MODEL>(from m in Model select new T_VIEW_MODEL() { Model = m })
                : null;
        }

    }

}
