﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using Xamarin.Forms.Xaml;
using Eliminacode.viewmodels;

namespace Eliminacode
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppView : ContentPage
	{
		public AppView() {
			InitializeComponent();
        }

        #region VIEW-MODEL
            
        public AppViewModel ViewModel {
            get => BindingContext as AppViewModel;
            set => BindingContext = value;
        }

        #endregion


        #region EVENTS [VIEW -> VIEW-MODEL]

        private void ServiceCollectionView_Select(object sender, EventArgsServiceVM e) {
            ViewModel.ServiceSelect = e.ServiceVM;
        }

        private void ServiceDetailView_Remove(object sender, EventArgsServiceVM e) {
            ViewModel.ServiceRemoveCommand.Execute(e.ServiceVM);
        }

        private void ServiceDetailView_Save(object sender, EventArgsServiceVM e) {
            ViewModel.ServiceSaveCommand.Execute(e.ServiceVM);
        }

        private void ServiceDetailView_Cancel(object sender, EventArgsServiceVM e) {
            ViewModel.ServiceCancelCommand.Execute(e.ServiceVM);
        }

        #endregion

    }
}
