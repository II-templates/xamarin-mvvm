﻿using Eliminacode.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Eliminacode
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ServiceCollectionView : ContentView
	{

        #region CONSTRUCTORS

        public ServiceCollectionView () {
			InitializeComponent ();
        }

        #endregion



        #region BINDABLE PROPERTIES [VIEW-MODEL <-> MODEL]

        public static readonly BindableProperty TitleProperty = BindableProperty.Create(
            propertyName: nameof(Title),
            returnType: typeof(string),
            declaringType: typeof(ServiceCollectionView),
            defaultValue: default(string),
            defaultBindingMode: BindingMode.OneWay,
            propertyChanged: (BindableObject bindable, object oldValue, object newValue) => {
                //if (bindable is ServiceCollectionView sv) sv.lblTitle.Text = (string)newValue;
            }
        );
        public string Title {
            get => (string)GetValue(TitleProperty);
            set {
                SetValue(TitleProperty, value);
            }
        }

        #endregion



        #region EVENTS

        public event EventHandler<EventArgsServiceVM> Select;

        private void Service_Clicked(object sender, EventArgs e) {
            // modifico il ViewModel
            var serviceVMSelected = (sender as BindableObject)?.BindingContext as ServiceViewModel;
            (BindingContext as ServiceCollectionViewModel).ServiceSelect = serviceVMSelected;

            // lancio l'evento di avvenuta selezione
            Select?.Invoke(this, new EventArgsServiceVM(serviceVMSelected));
        }

        #endregion

    }

    public class EventArgsServiceVM : EventArgs {
        public ServiceViewModel ServiceVM { get; private set; }
        public EventArgsServiceVM(ServiceViewModel serviceVM) : base() {
            ServiceVM = serviceVM;
        }
    }

}