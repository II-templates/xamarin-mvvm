﻿using Eliminacode.viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Eliminacode
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ServiceDetailView : ContentView
	{
		public ServiceDetailView ()
		{
			InitializeComponent ();
		}



        #region EVENTS

        public event EventHandler<EventArgsServiceVM> Remove;
        public event EventHandler<EventArgsServiceVM> Save;
        public event EventHandler<EventArgsServiceVM> Cancel;

        private void Remove_Clicked(object sender, EventArgs e) {
            Remove?.Invoke(this, new EventArgsServiceVM(BindingContext as ServiceViewModel));
        }

        private void Save_Clicked(object sender, EventArgs e) {
            Save?.Invoke(this, new EventArgsServiceVM(BindingContext as ServiceViewModel));
        }
        
        private void Cancel_Clicked(object sender, EventArgs e) {
            Cancel?.Invoke(this, new EventArgsServiceVM(BindingContext as ServiceViewModel));
        }

        #endregion
    }

}