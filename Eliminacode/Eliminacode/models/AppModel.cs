﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eliminacode.models
{
    public class AppModel : ICloneable {

        public string Title { get; set; }

        public IList<Service> Services { get; set; }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
