﻿using Eliminacode.repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eliminacode.models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Service : IEntity, ICloneable {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        public object Clone() => this.MemberwiseClone();

    }
}
