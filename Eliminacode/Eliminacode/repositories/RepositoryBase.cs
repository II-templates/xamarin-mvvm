﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Eliminacode.repositories
{
    public class RepositoryBase <T>  where T: new() {
        //IQueryable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate);

        public static async Task<string> HTTPGetAsync ( string url ) {

            return @"[ 
                { id:1, name: 'Ivano' },
                { id:2, name: 'Eduardo' },
                { id:3, name: 'Alfredo' } 
            ]";
            
            //string content;
            //var request = HttpWebRequest.Create( url );
            //    request.ContentType = "application/json";
            //    request.Method = "GET";
            //using (HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse) {
            //    if (response.StatusCode != HttpStatusCode.OK) Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
            //    using (StreamReader reader = new StreamReader(response.GetResponseStream())) {
            //        content = await reader.ReadToEndAsync();
            //        if (string.IsNullOrWhiteSpace(content)) {
            //            Console.Out.WriteLine("Response contained empty body...");
            //        } else {
            //            Console.Out.WriteLine("Response Body: \r\n {0}", content);
            //        }
            //    }
            //}
            //return content;

            //var client = new HttpClient();
            //client.DefaultRequestHeaders.Add("Accept", "application/json");
            //var address = "http://http://localhost:3000/users";
            //var response = await client.GetAsync(address);
            //var json = response.Content.ReadAsStringAsync().Result;
            ////var rootobject = JsonConvert.DeserializeObject<Rootobject>(airportJson);
            //return json;
        }



       


        public async Task<IList<T>> GetAll() {
            var jsonText = await RepositoryBase<T>.HTTPGetAsync("http://localhost:3000/users");
            return JsonConvert.DeserializeObject<List<T>>(jsonText);
        }

        public async Task<T> Create() {
            return new T();
        }

        public async Task Save(T entity) {
            return;
        }

        public async Task Remove(T entity) {
            return;
        }

    }

}
