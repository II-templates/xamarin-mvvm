﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eliminacode.repositories
{
    public interface IEntity {
        string Id {  get; set; }
    }
}
