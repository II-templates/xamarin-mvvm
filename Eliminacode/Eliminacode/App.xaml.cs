﻿using Eliminacode.models;
using Eliminacode.viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Eliminacode
{
	public partial class App : Application
	{

		public App ()
		{
			InitializeComponent();

            MainPage = new NavigationPage( 
                new AppView() { 
                    BindingContext = new AppViewModel() { 
                        Model = new AppModel() {
                            Title = "TITOLONE!!!",
                        }
                    }
                } 
            );
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
